package com.randiak_dev.test_visualizer;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import java.util.Arrays;

public class SimpleDrawingView extends View {
    // setup initial color
    private final int paintColor = Color.BLACK;
    // defines paint and canvas
    private Paint drawPaint;
    private int[] waveform;

    private int mDivisions = 1;
    private boolean mTop = true;
    private float[] mFFTPoints;

    public SimpleDrawingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setupPaint();
    }

    // Setup paint with color and stroke styles
    private void setupPaint() {
        drawPaint = new Paint();
        drawPaint.setColor(paintColor);
        drawPaint.setAntiAlias(true);
        drawPaint.setStrokeWidth(4);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeJoin(Paint.Join.ROUND);
        drawPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        int multiplier = 7;
//        if (waveform != null && waveform.length > 1) {
//            canvas.drawLine(0, 256, waveform.length * multiplier, 256, drawPaint);
//            for (int i = 0; i < waveform.length - 1; i++) {
//                canvas.drawLine(i * multiplier, (waveform[i] + 128),
//                        (i + 1) * multiplier, (waveform[i + 1] + 128), drawPaint);
//            }
//        }

//        int multiplier = 2;
//        if (waveform != null && waveform.length > 3) {
//            canvas.drawLine(0, 256, waveform.length * multiplier, 256, drawPaint);
//            for (int i = 0; i < waveform.length - 3; i++) {
//                canvas.drawLine(i * multiplier, (waveform[i]),
//                        i * multiplier, 0, drawPaint);
//            }
//        }
        if (mFFTPoints != null) {
            canvas.drawLines(mFFTPoints, drawPaint);
        }

    }

    public void setWaveform(int[] waveform) {
        this.waveform = Arrays.copyOf(waveform, waveform.length);
        invalidate();
    }

    public void setFFT(byte[] data, int gamma)
    {
        mFFTPoints = new float[data.length * 4];
        for (int i = 0; i < data.length / mDivisions - 1; i++) {
            mFFTPoints[i * 4] = i * 4 * mDivisions;
            mFFTPoints[i * 4 + 2] = i * 4 * mDivisions;
            byte rfk = data[mDivisions * i];
            byte ifk = data[mDivisions * i + 1];
            double magnitude = Math.pow((rfk * rfk + ifk * ifk), (1.0 / gamma));
            int dbValue = (int) (Math.pow(10, gamma) * Math.log10(magnitude));

            if(mTop)
            {
//                mFFTPoints[i * 4 + 1] = 0;
//                mFFTPoints[i * 4 + 3] = (dbValue * 2 - 10);
                mFFTPoints[i * 4 + 3] = (dbValue * 2 - 10);
                mFFTPoints[i * 4 + 3] = 0;
            }
//            else
//            {
//                mFFTPoints[i * 4 + 1] = rect.height();
//                mFFTPoints[i * 4 + 3] = rect.height() - (dbValue * 2 - 10);
//            }
        }
        invalidate();

    }

    public void setFFT(float[] data)
    {
        mFFTPoints = new float[data.length * 4];
        for (int i = 0; i < data.length / mDivisions; i++) {
            mFFTPoints[i * 4] = i * 4 * mDivisions;
            mFFTPoints[i * 4 + 2] = i * 4 * mDivisions;

            if(mTop)
            {
                mFFTPoints[i * 4 + 1] = data[i];

                                mFFTPoints[i * 4 + 3] = 0;
            }
//            else
//            {
//                mFFTPoints[i * 4 + 1] = rect.height();
//                mFFTPoints[i * 4 + 3] = rect.height() - (dbValue * 2 - 10);
//            }
        }
        invalidate();

    }
}
