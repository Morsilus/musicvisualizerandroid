package com.randiak_dev.test_visualizer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.audiofx.Visualizer;

import android.os.Bundle;
import android.util.Pair;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;


import com.gauravk.audiovisualizer.model.AnimSpeed;
import com.gauravk.audiovisualizer.visualizer.BarVisualizer;
import com.gauravk.audiovisualizer.visualizer.BlastVisualizer;

import org.eazegraph.lib.charts.BarChart;
import org.eazegraph.lib.charts.StackedBarChart;
import org.eazegraph.lib.models.BarModel;
import org.eazegraph.lib.models.StackedBarModel;

import java.sql.Time;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.abs;
import static java.lang.Math.exp;
import static java.lang.Math.log10;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class MainActivity extends AppCompatActivity
        implements Visualizer.OnDataCaptureListener {
    public Visualizer visualizer;
    private BarVisualizer blastVisualizer;
    private CustomVisualizer customVisualizer;
    private VisualizerDBsProcessing processing;
//    private VisualizerEngine visualizerEngine;
    private static final int CAPTURE_SIZE = 1024;
    private int min = 127;
    private Paint paint;
    private SimpleDrawingView canvas;
    private TextView tv1;
    private TextView tv2;
    private EditText et1;

    private SeekBar seekBar;

    private Rect mRect = new Rect();

    private int[] dividers = {140, 400, 2600, 5200, 22500};
    //{60, 250, 500, 2000, 4000, 6000, 20000};

    private Time time = new Time(0);
    private int peaks = 0;
    private float mean = 0;
    private int means = 1;

    BarChart chartSpectogram;

    private static final double START_F = 20;
    private static final double STOP_F = 22050;
    enum Distribution { LINEAR, LOGARITHMIC, MEL, OCT10, OCT32};
    Distribution distribution = Distribution.MEL;
    enum Weighting {NONE, A, B, C, D};
    Weighting weighting = Weighting.A;
    private float[] weightings;
    private float[] freqs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        paint = new Paint();
        canvas = findViewById(R.id.simpleDrawingView1);
        tv1 = findViewById(R.id.textView);
        tv2 = findViewById(R.id.textView2);
        seekBar = findViewById(R.id.seekBar);
//        visualizer = new Visualizer(0);
//        visualizer.setCaptureSize(CAPTURE_SIZE);
//        tv1.setText(String.valueOf(0));
        chartSpectogram = findViewById(R.id.chartSpectogram);
        weightings = new float[CAPTURE_SIZE / 2 + 1];
//        visualizer.setEnabled(true);
        customVisualizer = new CustomVisualizer(0);
        customVisualizer.setEnabled(true);
        processing = new VisualizerDBsProcessing();
        processing.setNSamples(10);
        customVisualizer.setVisualizeDataListener(new CustomVisualizer.OnVisualizeDataListener() {
            @Override
            public void onDBsDataCapture(Visualizer visualizer, Pair<Float, Float>[] freqDBs, int samplingRate) {
                float[] samples = processing.processDbData(freqDBs);
                String a = new String();
//                for (int i = 0; i < freqDBs.length; i++) {
//                    a = a.concat(String.valueOf(freqDBs[i].first) + " - " + String.valueOf(freqDBs[i].second) + "\n");
//                }
                tv2.setText(a);
                chartSpectogram.clearChart();
                chartSpectogram.addBar(new BarModel(22, 0x00000000));
                chartSpectogram.setVisibleBars(20 + 2);
                chartSpectogram.setBarWidth(getWindowManager().getDefaultDisplay().getWidth() / (20 + 2));

                for (int i = 0; i < samples.length; i++) {
//                    freqs = freqs.concat(String.valueOf(i) + " " + String.valueOf(samples[i]) + "\n");
                    StackedBarModel barModel = new StackedBarModel();
                    chartSpectogram.addBar(new BarModel(samples[i], 0xFFFF0000));
                }
            }

            @Override
            public void onPeakCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
//                tv1.setText("tu");
            }
        });
//        visualizerEngine = VisualizerEngine.getInstance(0);
//        visualizerEngine.startVisualizer();
//        blastVisualizer = findViewById(R.id.visualizer);
//        blastVisualizer.setAudioSessionId(0);
//        blastVisualizer.setAnimationSpeed(AnimSpeed.FAST);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        visualizerEngine = VisualizerEngine.getInstance(0);
//        visualizerEngine.startVisualizer();
//        startVisualiser();
//        blastVisualizer.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        blastVisualizer.release();
    }

    private void startVisualiser() {
//        visualizer.setDataCaptureListener(this, Visualizer.getMaxCaptureRate(), false, false);
        visualizer.setEnabled(true);
//        visualizer.setScalingMode(Visualizer.SCALING_MODE_NORMALIZED);
        visualizer.setMeasurementMode(Visualizer.MEASUREMENT_MODE_PEAK_RMS);

        visualizer.setScalingMode(Visualizer.SCALING_MODE_AS_PLAYED);
        calcFreqs(visualizer.getSamplingRate() / 1000, this.CAPTURE_SIZE);
    }

    private void calcFreqs(int rateHZ, int captureSize) {
        int nFreqs = captureSize / 2 + 1;
        freqs = new float[nFreqs];
        for (int i = 0; i < nFreqs; i++) {
            freqs[i] = (float) (i * rateHZ) / captureSize;
        }
    }

    @Override
    public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
//        for (int i = 0; i < waveform.length; i++) {
//            if (waveform[i] < min) {
//                min = waveform[i];
//
//                tv1.setText(String.valueOf(min));
//            }
//        }

//        canvas.setWaveform(waveform);
//        tv1.setText(String.valueOf(waveform.length));
//        tv1.setText(String.valueOf(visualizer.getSamplingRate()));
    }

    @Override
    public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        int volume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        double volumeCoef = (double)audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
                / audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        double debValue = 1;//Math.pow(10, volumeCoef) / 10;
        tv2.setText(String.valueOf(Visualizer.getMaxCaptureRate()));
        int n = visualizer.getCaptureSize();//fft.length;
//        float[] magnitudes = new float[n / 2 + 1];
        float[] magnitudes = new float[n / 2 + 1];
        float[] phases = new float[n / 2 + 1];
//        magnitudes[0] = (float)Math.abs(fft[0]);      // DC
//        magnitudes[n / 2] = (float)Math.abs(fft[1]);  // Nyquist
        magnitudes[0] = (float) (Math.abs(fft[0]) * debValue);      // DC
        magnitudes[n / 2] = (float) (Math.abs(fft[1]) * debValue);  // Nyquist
        phases[0] = phases[n / 2] = 0;

        float sumPhases = 0;
        for (int k = 1; k < n / 2; k++) {
            int i = k * 2;
//            magnitudes[k] = (float)Math.hypot(fft[i], fft[i + 1]) * 4 / 3;
            magnitudes[k] = (float) (Math.hypot(fft[i] , fft[i + 1]));///* * 4 / 3*/);
            phases[k] = (float)Math.atan2(fft[i + 1], fft[i]);
            sumPhases += phases[k];
            float real = fft[i];
            float imag = fft[i + 1];
//            magnitudes[k] = (float) (10 * log10(4 * (real * real + imag * imag) ));
        }

//        for (int k = 1; k < n / 2; k++) {
//            magnitudes[k] *= sumPhases;
//            magnitudes[k] = abs(magnitudes[k]);
//        }
        int freq = visualizer.getSamplingRate();

//        if (sumPhases >= CAPTURE_SIZE / 2) {
//            tv1.setText(String.valueOf(peaks));
//            time.getTime();
//            peaks++;
//        }
//        else
//            tv1.setText(String.valueOf(0));
//        canvas.setFFT(fft, 1);
//        canvas.setFFT(phases);
//        visualize(magnitudes, seekBar.getProgress(), n);
//        melDistribution(magnitudes, seekBar.getProgress());
//        debATest32(magnitudes);
//        normalDivide(magnitudes, n);
//        if (distribution == Distribution.OCT10) {
//            visualize(magnitudes, 10, n);
//        } else if (distribution == Distribution.OCT32) {
//            visualize(magnitudes, 32, n);
//        } else {
//            visualize(magnitudes, seekBar.getProgress(), n);
//        }
    }

//    @Override
//    public void onDBDataCapture(Visualizer visualizer, Pair<Float, Float>[] freqDBs, int samplingRate) {
//        String freqs = new String();
//        for (Pair<Float, Float> freqDB : freqDBs) {
//            freqs = freqs.concat(String.valueOf(freqDB.first) + " " + String.valueOf(freqDB.second) +  "\n");
//        }
//        tv2.setText(freqs);
//    }
//
//    @Override
//    public void onPeakCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
//
//    }
//
//    private void visualize(float[] magnitudes, int numSamples, int captureSize) {
//        float[] samples = new float[numSamples];
//        int index = 0;
////        float max =
//
////        magnitudes = aWeighting(magnitudes);
//        int rateHZ = visualizer.getSamplingRate() / 1000;
//        double divider = getDividerByDistribution(1, numSamples);
//        String freqs = new String();
//        for (int i = 0; i < magnitudes.length; i++) {
//            double freq = (double) ((i + 1) * rateHZ) / (captureSize);
//            if (freq > divider && index < numSamples - 1) {
//                index++;
//                divider = getDividerByDistribution(index + 1, numSamples);
//            } else {
//
////                float db = magnitudes[i];
//            float db = (float) (2 + 20 * Math.log10(magnitudes[i]));
//                if (weighting != Weighting.NONE) {
//                    db += getWeighting(i);
//                }
////            db = (float) (db + log10(1 - pow(10, -(db - aWeighting((float)freq)) / 10)));
//
//                if (samples[index] < db) {
//                    samples[index] = db;
//                }
//            }
//            freqs = freqs.concat(String.valueOf(weightings[i]) + " " + String.valueOf(divider) +
//                    " " + String.valueOf(this.freqs[i]) + " " + String.valueOf(samples[index]) + "\n");
//
//
//
//
//        }
//
//        chartSpectogram.clearChart();
//        chartSpectogram.addBar(new BarModel(48, 0x00000000));
//        chartSpectogram.setVisibleBars(numSamples + 2);
//        chartSpectogram.setBarWidth(getWindowManager().getDefaultDisplay().getWidth() / (numSamples + 2));
//
//        for (int i = 0; i < samples.length; i++) {
//            freqs = freqs.concat(String.valueOf(i) + " " + String.valueOf(samples[i]) + "\n");
//            StackedBarModel barModel = new StackedBarModel();
//            chartSpectogram.addBar(new BarModel(samples[i], 0xFFFF0000));
//        }
//
//        Visualizer.MeasurementPeakRms mPeakRms = new Visualizer.MeasurementPeakRms();
//        visualizer.getMeasurementPeakRms(mPeakRms);
//        tv1.setText(String.valueOf(mPeakRms.mPeak));
////        tv2.setText(String.valueOf(mPeakRms.mRms));
////        tv2.setText(freqs);
////        canvas.setFFT(samples);
//    }
//
//
//
//    public final static double[] denominator = new double[]{ 1.0,
//            -4.0195761811158306,
//            6.1894064429206894,
//            -4.4531989035441137,
//            1.420842949621872,
//            -0.14182547383030505,
//            0.004351177233494978};
//
//
//    public final static double[] numerator = new double[]{ 0.25574112520425768,
//            -0.51148225040851569,
//            -0.25574112520425829,
//            1.0229645008170301,
//            -0.25574112520425829,
//            -0.51148225040851569,
//            0.25574112520425768};
//
//    public static float[] aWeightingSignal(float[] inputSignal) {
//
//        int signalLength = inputSignal.length;
//        int order = Math.max(denominator.length, numerator.length);
//        float[] weightedSignal = new float[signalLength];
//        // Filter delays
//        float[][] z = new float[order-1][signalLength];
//
//        for (int idT = 0; idT < signalLength; idT++){
//            // Avoid iteration idT=0 exception (z[0][idT-1]=0)
//            weightedSignal[idT] = (float)(numerator[0]*inputSignal[idT] + (idT == 0 ? 0 : z[0][idT-1]));
//            // Avoid iteration idT=0 exception (z[1][idT-1]=0)
//            z[0][idT] = (float)(numerator[1]*inputSignal[idT] + (idT == 0 ? 0 : z[1][idT-1]) - denominator[1]*inputSignal[idT]);
//            for (int k = 0; k<order-2; k++){
//                // Avoid iteration idT=0 exception (z[k+1][idT-1]=0)
//                z[k][idT] = (float)(numerator[k+1]*inputSignal[idT] + (idT ==0 ? 0 : z[k+1][idT-1]) - denominator[k+1]*weightedSignal[idT]);
//            }
//            z[order-2][idT] = (float)(numerator[order-1]*inputSignal[idT] - denominator[order-1]*weightedSignal[idT]);
//        }
//        return weightedSignal;
//    }
//
//    private void melDistribution(byte[] magnitudes, int numSamples) {
//        byte[] samples = new byte[numSamples];
//        int index = 0;
//        String freqs = new String();
//
//        int rateHZ = visualizer.getSamplingRate() / 1000;
//        int captureSize = visualizer.getCaptureSize();
////        double divider = hertzToMels(logspace(20, rateHZ / 2, 1, numSamples));
//        double divider = logSpace(hertzToMels(20), hertzToMels(rateHZ / 2), 1, numSamples);
//        for (int i = 0; i < magnitudes.length; i++) {
//            double freq = hertzToMels((i * rateHZ) / captureSize);
//
////            freqs = freqs.concat(String.valueOf(freq) + " " + divider + "\n");
////            double freq = (i * rateHZ) / captureSize;
//            if (samples[index] < magnitudes[i]) {
//                samples[index] = magnitudes[i];
//            }
////            samples[index] += magnitudes[i];
//
//            if (freq > divider && index < numSamples - 1) {
////                freqs = freqs.concat(String.valueOf(index) + " " + String.valueOf(samples[index]) + " " + divider + "\n");
//                index++;
////                divider = hertzToMels(logspace(20, rateHZ / 2, index + 1, numSamples));
//                divider = logSpace(hertzToMels(20), hertzToMels(rateHZ / 2), index + 1, numSamples);
//            }
//        }
//        chartSpectogram.clearChart();
//        chartSpectogram.addBar(new BarModel(255, 0x00000000));
//        chartSpectogram.setVisibleBars(numSamples);
//        chartSpectogram.setBarWidth(getWindowManager().getDefaultDisplay().getWidth() / (numSamples + 1));
//
//        for (int i = 0; i < samples.length; i++) {
//            chartSpectogram.addBar(new BarModel(samples[i], 0xFFFF0000));
//        }
//        tv2.setText(freqs);
////        tv2.setText(String.valueOf(visualizer.getCaptureSize()));
//    }
//
//    private void normalDivide(float[] magnitudes, int captureSize) {
//        int divider = dividers[0];
//        int index = 0;
//        int rateHZ = visualizer.getSamplingRate() / 2 / 1000;
//        String freqs = new String();
//        float[] samples = new float[dividers.length];
//        for (int i = 0; i < magnitudes.length; i++) {
//            double freq = (double) (i * rateHZ) / captureSize;
//            if (samples[index] < magnitudes[i]) {
//                samples[index] = magnitudes[i];
//            }
//            if (freq > divider && index < dividers.length - 1) {
//                index++;
//                divider = dividers[index];
//
//            }
////            freqs = freqs.concat(String.valueOf(divider) + " " + String.valueOf(samples[index]) + "\n");
//            freqs = freqs.concat(String.valueOf(i) + " " + String.valueOf(divider) +
//                    " " + String.valueOf(freq) + " " + String.valueOf(index) + "\n");
////            samples[index] += magnitudes[k];
//        }
//        for (int i = 0; i < samples.length; i++) {
////            freqs = freqs.concat(String.valueOf(dividers[i]) + " " + String.valueOf(samples[i]) + "\n");
//        }
//
//        tv2.setText(freqs);
//        canvas.setFFT(samples);
//    }
//
//    // start - start frequency
//    // stop - stop frequency
//    // n - the point which you wish to compute (zero based)
//    // N - the number of points over which to divide the frequency range.
//
//
////    private double[] dbA(double data[]) {
////
////    }
//
//    private void debATest10(float[] magnitudes) {
//        int rateHZ = visualizer.getSamplingRate() / 1000;
//        int captureSize = visualizer.getCaptureSize();
//
//        double divs[] = {31.25, 62.5, 125, 250, 500, 1000, 2000, 4000, 8000, 16000};
//        double dbA[] = {-39.4, -26.2, -16.1, -8.6, -3.2, 0, 1.2, 1.0, -1.1, -6.6};
//        double dbB[] = {-17.1, -9.3, -4.2, -1.3, -0.3, 0, -0.1, -0.7, -2.9, -8.4};
//        int numSamples = 10;
//
//        float[] samples = new float[numSamples];
//        int index = 0;
//        int n = 0;
//        double divider = (divs[index]);
//        for (int i = 0; i < magnitudes.length; i++) {
//            double freq = ((double) (i * rateHZ) / captureSize);
//            if (freq > divider && index < numSamples - 1) {
////                samples[index] /= n;
//                index++;
//                divider = (divs[index]);
//                n = 0;
//            }
//            float db = (float) (2 + 20 * Math.log10(magnitudes[i]));
////            db = (float) (db + log10(1 - pow(10, -(db - dbA[index]) / 10)));
//            if (samples[index] < db) {
//                samples[index] = db;
//            }
////            samples[index] += db;
////            n++;
//        }
////        samples[index] /= n;
//
//        for (index = 0; index < numSamples; index++) {
//            samples[index] += dbB[index];
//        }
//
//        int max = 48;
//        chartSpectogram.clearChart();
//        chartSpectogram.addBar(new BarModel(max, 0x00000000));
//        chartSpectogram.setVisibleBars(numSamples + 2);
//        chartSpectogram.setBarWidth(getWindowManager().getDefaultDisplay().getWidth() / (numSamples + 3));
//
//        for (int i = 0; i < numSamples; i++) {
//            StackedBarModel barModel = new StackedBarModel();
//            chartSpectogram.addBar(new BarModel((float)samples[i], 0xFFFF0000));
//        }
//
//        chartSpectogram.addBar(new BarModel(max, 0x00000000));
//    }
//
//    private void debATest32(float[] magnitudes) {
//        int rateHZ = visualizer.getSamplingRate() / 1000;
//        int captureSize = visualizer.getCaptureSize();
//
//        int numSamples = 32;
//        double divs[] = new double[numSamples];
//        for (int i = 0; i < numSamples; i++) {
//            divs[i] = pow(2, (double)(i + 1) / 3) * 15.625;
//        }
//        double dbA[] = {/*-63.4,*/ -56.7, -50.5, -44.7, -39.4, -34.6, -30.2, -26.2, -22.5,
//                -19.1, -16.1, -13.4, -10.9, -8.6, -6.6, -4.8, -3.2,
//                -1.9, -0.8, 0, 0.6, 1.0, 1.2, 1.3, 1.2,
//                1.0, 0.5, -0.1, -1.1, -2.5, -4.3, -6.6, -9.3};
//
//        double dbB[] = {/*-33.2,*/ -28.5, -24.2, -20.4, -17.1, -14.2, -11.6, -9.3, -7.4,
//                -5.6, -4.2, -3.0, -2.0, -1.3, -0.8, -0.5, -0.3,
//                -0.1, 0, 0, 0, 0, -0.1, -0.2, -0.4,
//                -0.7, -1.2, -1.9, -2.9, -4.3, -6.1, -8.4, -11.1};
//
//        float[] samples = new float[numSamples];
//        int index = 0;
//        int n = 0;
//        double divider = (divs[index]);
//        for (int i = 0; i < magnitudes.length; i++) {
//            double freq = ((double) (i * rateHZ) / captureSize);
//            float db = (float) (2 + 20 * Math.log10(magnitudes[i]));
////            db = (float) (db + log10(1 - pow(10, -(db - 2* dbA[index]) / 10)));
//            if (samples[index] < db) {
//                samples[index] = db;
//            }
//            n++;
////            samples[index] += db;
//            if (freq > divider && index < numSamples - 1) {
////                samples[index] = samples[index] / n;
//                index++;
//                divider = (divs[index]);
//                n = 0;
//            }
//        }
//
////        samples[index] = samples[index] / n;
//        for (index = 0; index < numSamples; index++) {
//            samples[index] += dbB[index];
//        }
//
//        int max = 48;
//        chartSpectogram.clearChart();
//        chartSpectogram.addBar(new BarModel(max, 0x00000000));
//        chartSpectogram.setVisibleBars(numSamples + 2);
//        chartSpectogram.setBarWidth(getWindowManager().getDefaultDisplay().getWidth() / (numSamples + 6));
//
//        for (int i = 0; i < numSamples; i++) {
//            StackedBarModel barModel = new StackedBarModel();
//            chartSpectogram.addBar(new BarModel((float)samples[i], 0xFFFF0000));
//        }
//
//        chartSpectogram.addBar(new BarModel(max, 0x00000000));
//    }
}
