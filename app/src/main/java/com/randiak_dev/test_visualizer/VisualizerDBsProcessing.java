package com.randiak_dev.test_visualizer;

import android.util.Pair;

import static java.lang.Math.exp;
import static java.lang.Math.log10;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

public class VisualizerDBsProcessing {
    private static final double START_F = 20;
    private static final double STOP_F = 22050;

    enum Distribution { LIN, LOG, MEL, OCT10, OCT32};
    Distribution distribution = Distribution.LIN;
    enum Weighting {NONE, A, B, C, D};
    Weighting weighting = Weighting.A;
    private float[] weightings;

    private int nSamples;

    public VisualizerDBsProcessing() {
    }

    public void setDistribution(Distribution distribution) {
        this.distribution = distribution;
    }

    public void setWeighting(Weighting weighting) {
        this.weighting = weighting;
    }

    public void setNSamples(int nSamples) {
        this.nSamples = nSamples;
    }

    public float[] processDbData(Pair<Float, Float>[] freqDBs) {
        if (weighting != Weighting.NONE &&
                (weightings == null || weightings.length != freqDBs.length)) {
            weightings = new float[freqDBs.length];
            float[] freqs = new float[freqDBs.length];
            for (int i = 0; i < freqDBs.length; i++) {
                freqs[i] = freqDBs[i].first;
            }
            calcWeightings(freqs);
        }

        float[] samples = new float[nSamples];
        int index = 0;
        float divider = (float) getDividerByDistribution(index, nSamples);
        for (int i = 0; i < freqDBs.length; i++) {
            if (freqDBs[i].first > divider && index < nSamples - 1) {
                index++;
                divider = (float) getDividerByDistribution(index, nSamples);
            }
            float db = freqDBs[i].second;
            if (weighting != Weighting.NONE) {
                db += getWeighting(i);
            }

            if (samples[index] < db) {
                samples[index] = db;
            }
        }
        return samples;
    }

    private double getDividerByDistribution(int n, int N) {
        double divider = 0.;
        switch (distribution) {
            case LIN:
                divider = linSpace(START_F, STOP_F, n, N);
                break;
            case LOG:
                divider = logSpace(START_F, STOP_F, n, N);
                break;
            case MEL:
                divider = melSpace(START_F, STOP_F, n, N);
                break;
            case OCT10:
                divider = oct10Space(n);
                break;
            case OCT32:
                divider = oct32Space(n);
                break;
        }
        return divider;
    }

    private double linSpace(double start, double stop, int n, int N)
    {
        return (stop - start) / N * n + start;
    }

    private double logSpace(double start, double stop, int n, int N)
    {
        return start * Math.pow(stop / start, (n / (N - 1.)));
    }

    private double melSpace(double start, double stop, int n, int N) {
        double freqMels = linSpace(hertzToMels(start), hertzToMels(stop), n, N);
        return melsToHertz(freqMels);
    }

    private double hertzToMels(double frequency) {
        return 1127 * Math.log(1 + frequency / 700);
    }

    private double melsToHertz(double frequency) {
        return 700 * (exp(frequency / 1127) - 1);
    }

    private double oct10Space(int n) {
        return pow(2, n + 1) * 15.625;
    }

    private double oct32Space(int n) {
        return pow(2, (double)(n + 1) / 3) * 15.625;
    }

    private double oct64Space(int n) {
        return pow(2, (double)(n + 1) / 6) * 15.625;
    }

    private float getWeighting(int index) {
        if (index >= weightings.length) {
            return 0;
        }
        return weightings[index];
    }

    private void calcWeightings(float[] freqs) {
        switch (weighting) {
            case A:
                weightings = aWeighting(freqs);
                break;
            case B:
                weightings = bWeighting(freqs);
                break;
            case C:
                weightings = cWeighting(freqs);
                break;
        }
    }

    private float[] aWeighting(float[] freqs) {
        float[] lWeightings = new float[freqs.length];
        for (int i = 0; i < freqs.length; i++) {
            lWeightings[i] = (float)(20 * log10((pow(12194, 2) * pow(freqs[i], 4)) /
                    ((pow(freqs[i], 2) + pow(20.6, 2)) * (pow(freqs[i], 2) + pow(12194, 2)) *
                            sqrt(pow(freqs[i], 2) + pow(107.7, 2)) * sqrt(pow(freqs[i], 2) + pow(737.9, 2)))) + 2.);
        }
        return lWeightings;
    }

    private float[] bWeighting(float[] freqs) {
        float[] lWeightings = new float[freqs.length];
        for (int i = 0; i < freqs.length; i++) {
            lWeightings[i] = (float)(20 * log10((pow(12194, 2) * pow(freqs[i], 3)) /
                    ((pow(freqs[i], 2) + pow(20.6, 2)) * (pow(freqs[i], 2) + pow(12194, 2)) *
                            sqrt(pow(freqs[i], 2) + pow(158.5, 2)))) + 0.17);
        }
        return lWeightings;
    }

    private float[] cWeighting(float[] freqs) {
        float[] lWeightings = new float[freqs.length];
        for (int i = 0; i < freqs.length; i++) {
            lWeightings[i] = (float)(20 * log10((pow(12194, 2) * pow(freqs[i], 2)) /
                    ((pow(freqs[i], 2) + pow(20.6, 2)) * (pow(freqs[i], 2) + pow(12194, 2)))) + 0.06);
        }
        return lWeightings;
    }

    private float[] dWeighting(float[] freqs) {
        float[] lWeightings = new float[freqs.length];
        for (int i = 0; i < freqs.length; i++) {
            lWeightings[i] = (float)(20 * log10((pow(12194, 2) * pow(freqs[i], 4)) /
                    ((pow(freqs[i], 2) + pow(20.6, 2)) * (pow(freqs[i], 2) + pow(12194, 2)) *
                            sqrt(pow(freqs[i], 2) + pow(107.7, 2)) * sqrt(pow(freqs[i], 2) + pow(737.9, 2)))) + 2.);
        }
        return lWeightings;
    }
}
